//
//  ViewController.h
//  BubbleAnimation
//
//  Created by Wendy Abrantes on 08/04/2013.
//  Copyright (c) 2013 Wendy Abrantes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BubbleRailView.h"

@interface ViewController : UIViewController
{
    int bubbleNumber;
    BubbleRailView *bubbleRailView;
}
@end
