//
//  BubbleRailView.h
//  BubbleAnimation
//
//  Created by Wendy Abrantes on 08/04/2013.
//  Copyright (c) 2013 Wendy Abrantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BubbleRailView : UIView
{
    int nbBubble;
    CGFloat radiusSmallBubble;
    CGFloat radiusLargeBubble;
    CGFloat pipeWidth;

    UIImageView *backgroundPipeImageView;

    UIImage *bubbleImage;
    NSMutableArray *bubbleArray;
    
    
    UIView *containerView;
    
}

-(id)initWithFrame:(CGRect)paramFrame
      bubbleNumber:(int)paramBubbleNumber
 radiusSmallBubble:(CGFloat)paramRadiusSmallBubble
 radiusLargeBubble:(CGFloat)paramRadiusLargeBubble
         pipeWidth:(CGFloat)paramPipeWidth;

-(void)animateBubbleFromValue:(int)paramFromValue
                      toValue:(int)paramToValue;
@end
