//
//  BubbleRailView.m
//  BubbleAnimation
//
//  Created by Wendy Abrantes on 08/04/2013.
//  Copyright (c) 2013 Wendy Abrantes. All rights reserved.
//

#import "BubbleRailView.h"
#import "BubbleView.h"
#import <QuartzCore/QuartzCore.h>

const CGFloat pipeMargin = 7;

@implementation BubbleRailView

-(id)initWithFrame:(CGRect)paramFrame
      bubbleNumber:(int)paramBubbleNumber
 radiusSmallBubble:(CGFloat)paramRadiusSmallBubble
 radiusLargeBubble:(CGFloat)paramRadiusLargeBubble
         pipeWidth:(CGFloat)paramPipeWidth
{
    self = [super initWithFrame:paramFrame];
    if(self)
    {
        nbBubble = paramBubbleNumber;
        radiusSmallBubble = paramRadiusSmallBubble;
        radiusLargeBubble = paramRadiusLargeBubble;
        pipeWidth = paramPipeWidth;
        
        bubbleArray = [[NSMutableArray alloc] init]; 
        bubbleImage = [UIImage imageNamed:@"backgroundStartScreensStep"];
        
        CGFloat containerViewWidth = paramBubbleNumber*(radiusSmallBubble*2)+ (paramBubbleNumber-1)*paramPipeWidth;
         
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0, (self.frame.size.height - (radiusSmallBubble*2))/2, containerViewWidth, radiusSmallBubble*2)];
        backgroundPipeImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"backgroundStartScreensPipe"] stretchableImageWithLeftCapWidth:10 topCapHeight:10]];
        [backgroundPipeImageView setFrame:CGRectMake(0, (containerView.frame.size.height - 20)/2, containerView.frame.size.width, 20)];
        [containerView addSubview:backgroundPipeImageView];

        [self addSubview:containerView];
        [self createInterface];
    }
    return self;
}

-(void)createInterface
{
    CGFloat nextbubblePosition = radiusSmallBubble*2 + pipeWidth;
    
    for(int i=0; i< nbBubble; i++)
    {
        BOOL maskLeft = (i == 0)? YES : NO;
        BOOL maskRight = (i == nbBubble-1)? YES : NO;
        
        BubbleView *bubbleView = [[BubbleView alloc]
                                  initWithFrame:CGRectMake(nextbubblePosition*i, 0, radiusSmallBubble*2, radiusSmallBubble*2)
                                  isLeftMask:maskLeft
                                  isRightMask:maskRight];
        [bubbleArray addObject:bubbleView];
        [containerView addSubview:bubbleView];
    }
}

-(void)animateBubbleFromValue:(int)paramFromValue
                      toValue:(int)paramToValue
{
    BubbleView *bubbleViewToExpand = [bubbleArray objectAtIndex:paramToValue];
    BubbleView *bubbleViewToShrink;
    if(paramFromValue == -1)
    {
        bubbleViewToShrink = [bubbleArray objectAtIndex:(nbBubble-1)];
    }else{
        bubbleViewToShrink = [bubbleArray objectAtIndex:paramFromValue];
    }

    //SHRINK ANIMATION
    CGRect oldBounds1 = bubbleViewToShrink.layer.bounds;
    CGRect newBounds1 = oldBounds1;
    newBounds1.size = CGSizeMake(radiusSmallBubble*2, radiusSmallBubble*2);
    //if already shrink
    if(newBounds1.origin.y != 0 )
    {
        newBounds1.origin = bubbleViewToShrink.initialPoint;
    }
    CABasicAnimation *animationShrink = [CABasicAnimation animationWithKeyPath:@"bounds"];

    animationShrink.fromValue = [NSValue valueWithCGRect:oldBounds1];
    bubbleViewToShrink.layer.bounds = newBounds1;
    animationShrink.toValue = [NSValue valueWithCGRect:newBounds1];
    [bubbleViewToShrink.layer addAnimation:animationShrink forKey:@"expand"];

    
    //EXPAND ANIMATION
    CGRect oldBounds = bubbleViewToExpand.layer.bounds;
    CGRect newBounds = oldBounds;
    newBounds.size = CGSizeMake(radiusLargeBubble*2, radiusLargeBubble*2);
    newBounds.origin = CGPointMake(bubbleViewToExpand.initialPoint.x - (radiusLargeBubble - radiusSmallBubble), bubbleViewToExpand.initialPoint.y - (radiusLargeBubble - radiusSmallBubble));
    CABasicAnimation *animationExpand = [CABasicAnimation animationWithKeyPath:@"bounds"];
    
    // iOS
    animationExpand.fromValue = [NSValue valueWithCGRect:oldBounds];
    animationExpand.toValue = [NSValue valueWithCGRect:newBounds];
    bubbleViewToExpand.layer.bounds = newBounds;
    
    [bubbleViewToExpand.layer addAnimation:animationExpand forKey:@"expand"];
    
}

@end
