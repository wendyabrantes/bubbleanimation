//
//  AppDelegate.h
//  BubbleAnimation
//
//  Created by Wendy Abrantes on 08/04/2013.
//  Copyright (c) 2013 Wendy Abrantes. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
