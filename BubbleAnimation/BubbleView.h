//
//  BubbleView.h
//  BubbleAnimation
//
//  Created by Wendy Abrantes on 08/04/2013.
//  Copyright (c) 2013 Wendy Abrantes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BubbleView : UIView
{
    UIImageView *joinImageView;
    CGFloat joinMargin;
}

@property (nonatomic, assign) CGPoint initialPoint;
@property (nonatomic, strong) UIImageView *circleImageView;

//mask left or right pipe
-(id)initWithFrame:(CGRect)frame
        isLeftMask:(BOOL)paramIsLeftMask
       isRightMask:(BOOL)paramIsRightMask;

@end
