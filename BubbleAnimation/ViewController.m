//
//  ViewController.m
//  BubbleAnimation
//
//  Created by Wendy Abrantes on 08/04/2013.
//  Copyright (c) 2013 Wendy Abrantes. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    bubbleNumber = 4;
    bubbleRailView = [[BubbleRailView alloc] initWithFrame:CGRectMake(20, 100, 320, 200)
                                                              bubbleNumber:bubbleNumber
                                                         radiusSmallBubble:20
                                                         radiusLargeBubble:60
                                                                 pipeWidth:100];
    [self.view addSubview:bubbleRailView];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target:self selector:@selector(timerCallBack:) userInfo:nil repeats: YES];
}

-(void)timerCallBack:(NSTimer *)sender
{
    static int from = -1;
    static int to = 0;

    [bubbleRailView animateBubbleFromValue:from toValue:to];
    
    from++;
    to++;
    if(to == bubbleNumber)
    {
        from = -1;
        to = 0;
        
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
