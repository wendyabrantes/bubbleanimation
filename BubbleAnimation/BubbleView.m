//
//  BubbleView.m
//  BubbleAnimation
//
//  Created by Wendy Abrantes on 08/04/2013.
//  Copyright (c) 2013 Wendy Abrantes. All rights reserved.
//

#import "BubbleView.h"
#import <QuartzCore/QuartzCore.h>

@implementation BubbleView

-(id)initWithFrame:(CGRect)frame
        isLeftMask:(BOOL)paramIsLeftMask
       isRightMask:(BOOL)paramIsRightMask
{
    self = [super initWithFrame:frame];
    if(self)
    {
        joinMargin = 5;
        
        UIImage *joinImage = [UIImage imageNamed:@"backgroundStartScreensSliceJoin"];
        if(paramIsLeftMask || paramIsRightMask)
        {
            joinImage = [self createJoinImageWithMask:paramIsLeftMask];
        }
        
        joinImageView = [[UIImageView alloc] initWithFrame:CGRectMake(-joinMargin, (frame.size.height-20)/2, frame.size.width + 2*joinMargin, 20)];
        [joinImageView setImage:[joinImage stretchableImageWithLeftCapWidth:10 topCapHeight:20]];
        
        self.circleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.circleImageView setImage:[UIImage imageNamed:@"backgroundStartScreensStep"]];
        
        [self addSubview:self.circleImageView];
        [self insertSubview:joinImageView aboveSubview:self.circleImageView];
        
        self.initialPoint = CGPointMake(self.layer.bounds.origin.x,self.layer.bounds.origin.y);
    }
    return self;
}

-(void)layoutSublayersOfLayer:(CALayer *)layer
{
    [UIView animateWithDuration:0.3 animations:^{
        [joinImageView setBounds:CGRectMake(-joinMargin, (layer.bounds.size.height-20)/2, layer.bounds.size.width+ 2*joinMargin, 20)];
        self.circleImageView.bounds = layer.bounds;
    }];
}


-(UIImage *)createJoinImageWithMask:(BOOL)paramIsMaskLeft
{
    
    NSString *maskName;
    if(paramIsMaskLeft)
    {
        maskName = @"joinMaskLeft";
    }else{
        maskName = @"joinMaskRight";
    }
    
    //LOGO
    CGImageRef originalMask = [UIImage imageNamed:maskName].CGImage;
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(originalMask),
                                        CGImageGetHeight(originalMask),
                                        CGImageGetBitsPerComponent(originalMask),
                                        CGImageGetBitsPerPixel(originalMask),
                                        CGImageGetBytesPerRow(originalMask),
                                        CGImageGetDataProvider(originalMask), nil, YES);
    
    UIImage *image = [UIImage imageNamed:@"backgroundStartScreensSliceJoin"];
    CGImageRef maskedImageRef = CGImageCreateWithMask(image.CGImage, mask);
    
    UIImage *maskedImage = [UIImage imageWithCGImage:maskedImageRef scale:image.scale orientation:image.imageOrientation];
    
    
    CGImageRelease(mask);
    CGImageRelease(maskedImageRef);
    
    return maskedImage;
}

@end
